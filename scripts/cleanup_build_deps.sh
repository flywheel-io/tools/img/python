#!/bin/sh
# remove binaries from the final prod layer that trip trivy
set -eux
for bin in eget watchexec xh; do
    if [ -x "/opt/bin/$bin" ]; then
        rm -rf "/opt/bin/$bin"
    elif which apk >/dev/null 2>&1; then
        apk del "$bin"
    else
        apt-get remove "$bin"
    fi
done
