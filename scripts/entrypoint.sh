#!/bin/bash
# /entrypoint.sh - generic fw entrypoint for common container needs
PS4='+ ${BASH_SOURCE#$PWD\/}:$LINENO ${FUNCNAME:+$FUNCNAME(): }'
set -Eeuo pipefail
test -z "${DEBUG:-}" || set -x

# FW_LOGS - if set to '1', write container logs to daily-rotated files
if [[ "${FW_LOGS:-}" =~ 1|true ]]; then
    exec > >(rotatelogs -cDef "${FW_LOGS_DIR:-/data/logs}/%Y-%m-%d" 86400) 2>&1
fi
# FW_INIT - if set to <command|script>, eval it on container startup
if [[ -n "${FW_INIT:-}" ]]; then
    eval "$FW_INIT"
fi
# FW_RELOAD - if set to '1', restart the command on file-system changes (dev)
if [[ "${FW_RELOAD:-}" =~ 1|true ]]; then
    set -- watchexec -nr -- sh -c "${*@Q} || touch ."
fi
# FW_GOSU - if set to '1', step down from root to flywheel
if [[ "${FW_GOSU:-}" =~ 1|true && "$(id -un)" = root ]]; then
    set -- gosu flywheel "$@"
fi
# FW_TINI - unless set to '0', use tini for signal forwarding and zombie reaping
if [[ ! "${FW_TINI:-}" =~ 0|false ]]; then
    set -- tini -- "$@"
fi

# run the command in the main process pid using exec
exec "$@"
