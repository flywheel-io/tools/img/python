#!/bin/sh
set -eux
eget_url=https://github.com/zyedidia/eget/releases
eget_ver=$(wget -qSO- "$eget_url/latest" 2>&1 | sed -En 's| *Location:.*/tag/(.*)|\1|p')
eget_bin=linux_$(uname -m | sed -E 's|x86_|amd|;s|aarch|arm|')
wget -qO- "$eget_url/download/$eget_ver/eget-${eget_ver#v}-$eget_bin.tar.gz" \
    | tar xzm -C/opt/bin --no-same-owner --strip-components=1 "eget-${eget_ver#v}-$eget_bin/eget"
