# flywheel/python

Python base image for Flywheel projects, coming with

- the `flywheel` user and group - just set `USER flywheel` in the last layer
- the following build- and/or run-time utilities:
  - [`aria2`](https://github.com/aria2/aria2) - fast downloader
  - [`bash`](https://github.com/bminor/bash) - recommended shell for scripts
  - [`busybox`](https://busybox.net/about.html) - unix toolkit (`ps`, `ping`, etc.)
  - [`eget`](https://github.com/zyedidia/eget) - github pre-built binary installer
  - [`fd`](https://github.com/sharkdp/fd) - faster and better `find`
  - [`git`](https://github.com/git/git) - for installing deps from source
  - [`jo`](https://github.com/jpmens/jo) - emit json from cli args
  - [`jq`](https://github.com/itchyny/gojq) - json manipulation
  - [`micro`](https://github.com/zyedidia/micro) - dev-friendly text editor
  - [`openssl`](https://github.com/openssl/openssl) - TLS toolkit
  - [`rg`](https://github.com/BurntSushi/ripgrep) - faster and better `grep`
  - [`sd`](https://github.com/chmln/sd) - faster and better `sed`
  - [`ssh`](https://github.com/openssh/openssh-portable) - secure shell for git+ssh
  - [`templar`](https://github.com/proctorlabs/templar) - jinja2-style template cli
  - [`uv`](https://github.com/astral-sh/uv) - faster and better `pip`
  - [`watchexec`](https://github.com/watchexec/watchexec) - live reloader
  - [`xh`](https://github.com/ducaale/xh) - secure and dev-friendly `curl`
  - [`xz`](https://github.com/tukaani-project/xz) - general purpose commpression alg
  - [`yq`](https://github.com/mikefarah/yq) - yaml manipulation
  - [`zstd`](https://github.com/facebook/zstd) - fast, lossless compression alg

## Tags

CI pipelines on the `main` branch push the following tagging scheme to
[DockerHub][dockerhub]:

- `flywheel/python:{PYTHON_VER}-{BASE_IMAGE}`:
  the minimal recommended prod base image
- `flywheel/python:{PYTHON_VER}-{BASE_IMAGE}-build`:
  variant including `eget`, `watchexec`, `xh`

Available `PYTHON_VER`s: `3.13` (default), `3.12`, `3.11`, `3.10`, `3.9`<br/>
Available `BASE_IMAGE`s: `alpine` (default), `debian` (bookworm)

The default `3.13-alpine` is also tagged as `latest`, e.g.:

- `flywheel/python:latest` === `:3.13-alpine`
- `flywheel/python:build` === `:3.13-alpine-build`

Commit-specific tags are available for reproducibility, e.g.:

- `flywheel/python:3.13-debian-d34db33f`

[dockerhub]: https://hub.docker.com/repository/docker/flywheel/python/tags?page=1&ordering=last_updated

## Usage

Create a `.dockerignore` to keep the context limited to the files added:

```plaintext
# exclude everything
**
# include files needed
!myproj
!pyproject.toml
!README.md
!requirements.txt
# re-exclude cruft
**/__pycache__
```

Create a `Dockerfile`:

```docker
FROM flywheel/python:3.12-alpine AS base
SHELL ["/bin/bash", "-euxo", "pipefail", "-c"]
WORKDIR /src
COPY requirements.txt ./
RUN uv pip install -rrequirements.txt
CMD ["my-app"]
EXPOSE 8000

FROM base AS dev
COPY requirements-dev.txt ./
RUN uv pip install -rrequirements-dev.txt
COPY . .
RUN python -m compileall -bqj0 . && \
    fd -epy -E__init__.py -xrm . && \
    uv pip install --no-deps -e.

FROM base AS prod
COPY --from=dev /src .
RUN uv pip install --no-deps -e.
USER flywheel
```

## License

[![MIT](https://img.shields.io/badge/license-MIT-green)](LICENSE)
